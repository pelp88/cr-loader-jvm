package lab1;

import java.io.*;

public class CrClassLoader extends ClassLoader {

    /**
     * @param name имя класса для загрузки
     * @return экземпляр загруженного класса
     * @throws ClassNotFoundException не найден класс по указанному имени
     * @throws RuntimeException ошибки при загрузке класса
     */
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        var file = new File(name + ".class");

        if (!file.isFile())
            throw new ClassNotFoundException("No such class: " + name + ".class");

        try (var input = new BufferedInputStream(new FileInputStream(file))) {
            byte[] classBytes = new byte[(int) file.length()];
            input.read(classBytes);
            return defineClass(name, classBytes, 0, classBytes.length);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}